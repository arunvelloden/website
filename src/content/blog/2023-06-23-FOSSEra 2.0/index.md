---
path: /blog/FOSSEra 2.0
date: "2023-06-23"
datestring: "23-24 June 2023"
cover: "./Banner.png"
tag: "FOSSEra 2.0"
author: "Riyasasikumar"
name: "Riya S"
title: "FOSSEra 2.0"
desc: "2-day flagship event of FOSSNSS"
---

The FOSS Cell of NSS College of Engineering, Palakkad (FOSSNSS) community successfully hosted FOSSEra 2.0 on 23rd and 24th june 2023.FOSSEra 2.0 was conducted as a 2-day event comprising of a workshop and a hackathon. The first day featured a hands-on workshop on OpenStreetMap by Mr. Manoj Karingamadathil, followed by a 24-hour hackathon called HackFiesta v2.0 on the second day.With these events, Fossera 2.0 aimed to provide participants with practical knowledge and hands-on experience in open-source development. The event brought together students with a similar interest to explore the world of open source and created a vibrant community. Around 40 students attended the workshop and 8 teams participated in the hackathon. Students from various departments attended the event and it was an opportunity to showcase their skills, collaborate with peers and brainstorm innovative ideas.
![](./pic1.jpg)

![](./pic2.jpg)

## Day 1 (23/06/2023)

FOSSERA  2.0 Day 1 commenced with the inaugural ceremony at 9.15 am. Dr.Anuraj Mohan ( Staff Coordinator of FOSSNSS) delivered a welcome address along with an introductory speech briefing the importance of open source, encouraging students to make contributions to this field.The event was officially inaugurated by Dr.Maya Mohan (HOD - Dept. of Computer science And Engineering) who emphasized the need and role of  such events and motivated students to explore the skills within themselves,fostering their active participation.The inauguration concluded with a formal speech from both the speakers and marked the beginning of the hands-on session.

Day 1’s agenda was to conduct a hands-on workshop - ‘Introduction to open street map’.The session was handled by Mr.Manoj Karingamadathil. The aim of the session was to equip the participants with the basics of open street mapping and how to incorporate it when it comes to real world application.Since it was a generalized topic, all branch students found it interesting to follow and learn.The speaker presented a walkthrough of the website familiarizing the students with its environment, making it effortless and easy for them to work on. Each of the participants were asked to create an account on the site. He demonstrated the mapping of buildings in a locality which was a task handled on by his team and held it out as a challenge.Then there was a break at 12 pm. For the participants to gear up, a fun game session to solve some riddles were conducted.The winners were given chocolates.The session saw an energetic response from each and every one involved and remarked a successful ending at 3:30 pm.
Day 1 ended with the start of Hackfiesta v2.0. It was filled with learning, fun and collaborations. Delegates were allowed to build anything, ranging from a simple website to a complex web app. Participants were given the hackathon themes and notified about the rules.They were allowed to use any free software tools and expected to develop a prototype using a stack of their choice.


## Day 2 (24/06/2023)

Hackfiesta v2.0, the 24-hr extensive hackathon by FOSSNSS resumed with immense participation in the second day.There were about 32 participants who were divided into 8 groups, with 11 volunteers and 2 mentors. Alumni members Jayaraj J and Bageeradhan K H stood as mentors with immense zest to conduct the event efficiently. Subjects provided to the participants were climate and environment with each group selecting either of the two topics.Those who participated in the event developed projects and presentations based on the domain of their choice. The participants ideated solutions on the problems evoked from the subject chosen. Hands-on support from mentors and zeal of each and everyone throughout this event made it successful.It provided an insight to the technical world of softwares and the problems associated with the concerned domain.
The judgment panel had Dr.Maya Mohan and Dr.Anuraj Mohan from the department of Computer Science. All the teams presented their project and its prototype. Some questions arose from judges while executing the projects, which was perfectly handled by the students.It has empowered the students to enhance their skills to next level of programming and software development.Solutions for many problems related with current scenario and needs of normal people that are cost-effective,systematic implementation using software and generating prototype for the designs were encountered.It was a vibrant platform for the students to have effective communication, to share innovative ideas and improve their coding skills.
![](./pic3.jpg)
![](./pic4.jpg)


By 3:30 pm, the judges began to evaluate each team. The teams were evaluated based on the problem chosen from the given topic, solution relevance, code quality, and prototyping. The mentors were available throughout the hackathon to support the participants.There was an efficient interaction between the mentors and the participants, giving them advices, solution to the problems that were difficult to solve and provided encouragement to complete their specified tasks.The initial round of judgment completed with prototype description judged by the alumnus with their suggestions on refining the prototype.A brief description was given to them about the concept done in the presentation. The second round of judgment was done by Dr Maya Mohan and Dr Anuraj Mohan with the presentation as well as the prototype illustration.The teams were asked to present their idea, project implementation in front of other teams and evaluators.The judges spent a good amount of time interacting with each team, aiming to understand the ideas they cultivated and presented onto the screen. After productive and efficient hours of interaction and presentation, the final round of the hackathon came to an end with Dr Anuraj Mohan announcing the results of the hackathon along with his experience and insights during the evaluation period. The winning teams were awarded cash prizes having a total of INR 5000 and a certificate of appreciation. Swag packs and certificates of participation were given to all the participants. Finally, the event concluded with a vote of thanks, delivered by Mr Jayaraj J, Former Student coordinator of FOSSNSS thanking the sponsors, judges, and volunteers for their constant guidance and support in conducting the event effectively.

![](./grouppic.jpg)


Fossera 2.0 was a remarkable event organized by the FOSS Cell of NSSCE. Fossera had a profound impact on both the participants and the open-source community. Through the workshop, around 40 students gained a solid understanding of OpenStreetMap and its capabilities, empowering them to contribute to open-source mapping projects in the future. The exhilarating 24 hour hackathon provided the 8 teams a conducive environment for creativity and problem-solving. These events helped the students unveil their passion for open source. The event's commitment to being free ensured that cost was not a barrier for participants, allowing a diverse group of individuals to engage with open-source software and its possibilities. 


